package ru.god.matrix;

import java.util.ArrayList;

/**
 * Класс для создания матрицы со столбцами разной размерности
 *
 * @author Горбачева, 16ИТ18к
 */
public class Matrix {
    /*
    ArrayList, который включает в себя произвольное
    количество строковых массивов - столбцов матрицы
     */
    private static ArrayList<String[]> matrix = new ArrayList<>();
    /*
    Константа, являющаяся максимальным значением
    Причем как для количества строковых массивов (строк),
    так и для количества элементов каждого из этих массивов (столбцов)
     */
    private static final int MAX_SIZE = 10;

    public static void main(String[] args) {
        fillingMatrix();
        printMatrix();
    }

    /**
     * Метод, реализующий заполнение матрицы числами
     */
    private static void fillingMatrix() {
        /*
        Количество строковых массивов - столбцов матрицы
        Задается рандомно от 1 до значения максимальной размерности
         */
        int sizeOfArrayList = 1 + (int) (Math.random() * MAX_SIZE);
        for (int i = 0; i < sizeOfArrayList; i++) {
            int index;
            /*
            Количество числовых элементов строкового массива,
            то есть, количество строк в данном столбце
            Задается рандомно от 1 до значения максимальной размерности
             */
            int sizeOfArray = 1 + (int) (Math.random() * MAX_SIZE);
            /*
            Изначально каждый массив принимает величину, равную константе максимума
            Для корректного вывода матрицы на экран необходимо, чтобы
            каждый столбец имел фиксированное количество элементов
            Например, в случае, если в столбце n предусмотрено 6 строк,
            а в столбце n + 1 - больше 6, то программа не сможет
            обработать такую ситуацию, остановившись на 6-ой строке
             */
            String[] array = new String[MAX_SIZE];
            /*
            Далее массив заполняется пробелами
            Для удобства, чтобы на экране не появлялось выражение "null" там,
            где элемент не предусмотрен, весь массив изначально заполняетя пробелами
             */
            for (index = 0; index < MAX_SIZE; index++) {
                array[index] = " ";
            }
            /*
            Теперь в столбец добавляются строки: значения задаются рандомно
             */
            for (index = 0; index < sizeOfArray; index++) {
                array[index] = String.valueOf((int) (Math.random() * MAX_SIZE));
            }
            /*
            Добавление массива в ArrayList
             */
            matrix.add(array);
        }
    }

    /**
     * Метод, реализующий вывод матрицы на экран
     */
    private static void printMatrix() {
        for (int i = 0; i < MAX_SIZE; i++) {
            for (String[] array : matrix) {
                System.out.print(array[i] + "\t");
            }
            System.out.println();
        }
    }
}